var atributos = [
	document.querySelector('#date'),
	document.querySelector('#value'),
	document.querySelector('#description'),
	document.querySelector('#type')	
] 
var total = 0;
document.querySelector('#submitButton').addEventListener('click',submitButtonOnClickFunction,true);
function submitButtonOnClickFunction(event){
	event.preventDefault();

	var tbody = document.querySelector('table tbody');
	var tr = document.createElement('tr');

    atributos.forEach(function(atributo) {
        var td = document.createElement('td');
        td.textContent = atributo.value;
        tr.appendChild(td);
        if(atributo.id=='value'){
       		total = total + parseInt(atributo.value);
        }
    });
    tbody.appendChild(tr)
    
    adicionarTotal();
    
    atributos[0].value = '';
	atributos[1].value = '';
	atributos[2].value = '';
	atributos[3].value = '';

	atributos[0].focus();
}

function adicionarTotal(){
	var trfoot = document.querySelector('#tfoot')	
    if(!trfoot){
    	trfoot = document.createElement('tr');
    	trfoot.id = 'tfoot';

    	var tdLabel = document.createElement('td');
	    tdLabel.textContent = 'Total';
	    
	    var tdTotal = document.createElement('td');
	    tdTotal.id = "totalId";
	    tdTotal.textContent = total;

   	    trfoot.appendChild(tdLabel);
	    trfoot.appendChild(tdTotal);

	    var tfoot = document.querySelector('table tfoot');
	    tfoot.appendChild(trfoot)
    }else{
    	var trfoot = document.querySelector('#totalId')
    	console.log(trfoot.textContent)
    	console.log(total)

    	trfoot.textContent = total
    }
}