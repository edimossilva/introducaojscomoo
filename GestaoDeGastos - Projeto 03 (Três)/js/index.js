var atributos = [
	document.querySelector('#date'),
	document.querySelector('#value'),
	document.querySelector('#description'),
	document.querySelector('#type')	
]  

document.querySelector('#submitButton').addEventListener('click',submitButtonOnClickFunction);

var gastos = [];
function submitButtonOnClickFunction(event){
	event.preventDefault();
	var gasto = new Gasto(atributos);
    gastos.push(gasto);
    
    var tbody = document.querySelector('table tbody');   
    var tr = criarLinhaGasto(gasto);
    tbody.appendChild(tr)
    
    adicionarTotal();
    
    atributos[0].value = '';
	atributos[1].value = '';
	atributos[2].value = '';
	atributos[3].value = '';

	atributos[0].focus();
}

function criarLinhaGasto(gasto){
    var tr = document.createElement('tr');
    var tdData = criarColuna(gasto.data);
    var tdValor = criarColuna(gasto.valor);
    var tdDescricao = criarColuna(gasto.descricao);
    var tdTipo = criarColuna(gasto.tipo);
    tr.appendChild(tdData);
    tr.appendChild(tdValor);
    tr.appendChild(tdDescricao);
    tr.appendChild(tdTipo);
    return tr;
}

function criarColuna(valor){
    var td = document.createElement('td');
    td.textContent = valor;
    return td;
}

function calcularTotal(){
    var total = 0;
    gastos.forEach(function(gasto){
        total = total + parseInt(gasto.valor);
    })
    return total;
}

function adicionarTotal(){
	var trfoot = document.querySelector('#tfoot')	
    if(!trfoot){
    	trfoot = document.createElement('tr');
    	trfoot.id = 'tfoot';

    	var tdLabel = document.createElement('td');
	    tdLabel.textContent = 'Total';
	    
	    var tdTotal = document.createElement('td');
	    tdTotal.id = "totalId";
	    tdTotal.textContent = calcularTotal();

   	    trfoot.appendChild(tdLabel);
	    trfoot.appendChild(tdTotal);

	    var tfoot = document.querySelector('table tfoot');
	    tfoot.appendChild(trfoot)
    }else{
    	var trfoot = document.querySelector('#totalId')
    	console.log(trfoot.textContent)

    	trfoot.textContent = calcularTotal();
    }
}