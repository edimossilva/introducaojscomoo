class Gasto {
    constructor(atributos) {
        this._data = atributos[0].value;
        this._valor = parseInt(atributos[1].value);
        this._descricao = atributos[2].value;
        this._tipo = atributos[3].value;
    }

    get data() {
    	return this._data;
	}
	set data(data){
		this._data = data;
	}

	get valor() {
    	return this._valor;
	}
	set valor(valor){
		this._valor = valor;
	}

	get descricao() {
    	return this._descricao;
	}
	set descricao(descricao){
		this._descricao = descricao;
	}

	get tipo() {
    	return this._tipo;
	}
	set tipo(tipo) {
		this._tipo = tipo;
	}
}

